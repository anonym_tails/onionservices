from gi.repository import Gtk

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.panel import ServicePanel
    from onionservices.action_widget import ClickableRow


class GroupBox(object):
    def __init__(self, panel: "ServicePanel", group: str):
        self.rows = list()

        # Add group label
        self.label = Gtk.Label()
        self.label.set_markup('<span weight="bold">%s</span>' % group)
        self.label.set_hexpand(True)
        self.label.set_xalign(0)

        # Put the group label in a box, because sometimes we want to add another widget behind the label
        self.label_box = Gtk.Box()
        self.label_box.set_margin_bottom(12)
        self.label_box.add(self.label)

        # Add the group label box to the panel
        panel.options_box.add(self.label_box)

        # Add group frame
        self.frame = Gtk.Frame()
        self.frame.set_shadow_type(Gtk.ShadowType.IN)
        self.frame.set_margin_bottom(32)

        # Add a listbox to the frame
        self.listbox = Gtk.ListBox()
        self.listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.listbox.set_header_func(self.add_separator)
        self.listbox.connect("row-activated", self.on_row_activated)
        self.frame.add(self.listbox)

        # Add the group frame to the panel
        panel.options_box.add(self.frame)

    def add_separator(self, row, before, data=None):
        if not before:
            return
        separator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        row.set_header(separator)

    def add_row(self, row: "ClickableRow"):
        self.listbox.add(row.widget)
        self.rows.append(row)

    def on_row_activated(self, box: Gtk.ListBox, row: Gtk.ListBoxRow, user_data=None):
        for clickable_row in self.rows:
            if clickable_row.widget == row:
                clickable_row.on_activated()
                return
