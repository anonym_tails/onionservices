from logging import getLogger

from gi.repository import Gtk

from onionservices import _
from onionservices.config import APP_NAME, SERVICE_LIST_UI_FILE
from onionservices import util
from onionservices.service_chooser import ServiceChooserDialog
from onionservices.service_status import Status
from onionservices.warning_dialog import WarningDialog

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.service import Service


logger = getLogger(__name__)


class InvalidStatusError(Exception):
    pass


class ServiceList(object):

    def __init__(self, gui):
        self.gui = gui
        self.builder = Gtk.Builder()
        self.builder.set_translation_domain(APP_NAME)
        self.builder.add_from_file(SERVICE_LIST_UI_FILE)
        self.builder.connect_signals(self)
        self.listbox = self.builder.get_object("listbox")
        self.box = self.builder.get_object("box")
        self.remove_service_button = self.builder.get_object("toolbutton_remove_service")
        self.services = list()

    def __len__(self):
        return len(self.services)

    def __getitem__(self, item):
        return self.services[item]

    def get_service_by_listboxrow(self, listboxrow) -> "Service":
        return next(service for service in self.services if service.row.listboxrow == listboxrow)

    def get_selected_service(self) -> "Service":
        return self.get_service_by_listboxrow(self.listbox.get_selected_row())

    def add(self, service: "Service"):
        logger.debug("Adding service %r to service list", service.Name)
        self.services.append(service)
        self.listbox.add(ServiceRow(service).listboxrow)
        service.on_service_added()

        if len(self.services) == 1:
            self.select(service)
            # self.box.show()
        self.remove_service_button.set_sensitive(True)

    def remove(self, service: "Service"):
        logger.debug("Removing service %r from service list", service.Name)
        self.services.remove(service)
        self.listbox.remove(service.row.listboxrow)

        if self.services:
            self.select(self.services[0])
        else:
            self.gui.all_services_removed()
            self.remove_service_button.set_sensitive(False)

    def remove_all(self):
        for service in self.services:
            self.listbox.remove(service.row.listboxrow)
        self.services = list()
        self.gui.all_services_removed()
        self.remove_service_button.set_sensitive(False)

    def select(self, service: "Service"):
        self.listbox.select_row(service.row.listboxrow)
        service.on_service_selected()

    def on_button_add_service_clicked(self, button):
        ServiceChooserDialog(self.gui).run()

    def on_button_remove_service_clicked(self, button):
        service = self.get_selected_service()

        text = _("This will irrevocably delete all configuration and data of this service, "
                 "including the onion address.")

        warning = WarningDialog(
            parent=self.gui.window,
            title=_("Remove %s?") % service.NameForDisplay,
            text=text,
            button_title=_("Remove Service")
        )

        if warning.run() == Gtk.ResponseType.OK:
            util.run_threaded(service.uninstall)

    def on_row_activated(self, listbox, listboxrow):
        service = self.get_service_by_listboxrow(listboxrow)
        service.on_service_selected()


class ServiceRow(object):
    def __init__(self, service: "Service"):
        self.service = service
        service.row = self
        self.builder = Gtk.Builder()
        self.builder.set_translation_domain(APP_NAME)
        self.builder.add_from_file(SERVICE_LIST_UI_FILE)
        self.listboxrow = self.builder.get_object("listboxrow")
        label = self.builder.get_object("title_label")
        label.set_label(service.NameForDisplay)
        icon = self.builder.get_object("icon_image")
        _, size = icon.get_icon_name()
        icon.set_from_icon_name(service.Icon, size)

    def update(self):
        """Update the status widgets of all services in the service list"""
        box = self.builder.get_object("status_box")
        label = self.builder.get_object("status_label")

        for child in box.get_children():
            box.remove(child)

        visual_widget = self.get_status_widget(self.service.Status)
        label_value = self.get_label(self.service.Status)

        if visual_widget:
            box.pack_start(visual_widget, expand=False, fill=False, padding=0)
        if label_value:
            label.set_label(label_value)
            box.pack_start(label, expand=False, fill=False, padding=0)

    def get_status_widget(self, status):
        image = None
        if status in (Status.STARTING, Status.STOPPING, Status.INSTALLING, Status.RESTARTING,
                      Status.UNINSTALLING, Status.PUBLISHING, Status.NOT_INSTALLED):
            return None
        if status in (Status.ERROR, Status.ERROR_TOR_IS_NOT_RUNNING, Status.STOPPED_UNEXPECTEDLY):
            image = self.builder.get_object("image_error")
        if status in Status.STOPPED:
            image = self.builder.get_object("image_off")
        if status == Status.RUNNING:
            image = self.builder.get_object("image_on")
        if not image:
            raise InvalidStatusError("No visual widget for status %r defined" % status)
        image.set_pixel_size(16)
        return image

    @staticmethod
    def get_label(status):
        if status in (Status.STARTING, Status.STOPPING, Status.INSTALLING, Status.UNINSTALLING, Status.PUBLISHING):
            return "..."
        if status == Status.STOPPED:
            return _("Off")
        if status == Status.RUNNING:
            return _("On")
        if status in (Status.ERROR, Status.ERROR_TOR_IS_NOT_RUNNING, Status.STOPPED_UNEXPECTEDLY, Status.NOT_INSTALLED):
            return _("Error")
        return None
