from logging import getLogger
import subprocess
from gi.repository import Gtk, Gdk, Gio

from pydbus import SystemBus

from onionservices import services
from onionservices.service_list import ServiceList
from onionservices.service_chooser import ServiceChooserDialog
from onionservices.service import Service
from onionservices.service_status import Status

from onionservices.config import APP_NAME, ICON_DIR, WINDOW_UI_FILE, TAILS_USER, WINDOW_TITLE


# Only required for type hints
from typing import TYPE_CHECKING, List
if TYPE_CHECKING:
    from onionservices.service import ServicePanel


logger = getLogger(__name__)


class MainWindow(object):

    def __init__(self, tails_mode: bool):
        logger.debug("Initializing GUI")
        self.tails_mode = tails_mode

        self.builder = Gtk.Builder()
        self.builder.set_translation_domain(APP_NAME)
        self.builder.add_from_file(WINDOW_UI_FILE)
        self.builder.connect_signals(self)

        icon_theme = Gtk.IconTheme.get_default()
        icon_theme.prepend_search_path(ICON_DIR)

        self.window = self.builder.get_object("window")                             # type: Gtk.Window
        self.stack = self.builder.get_object("stack")                               # type: Gtk.Stack
        self.sidebar_headerbar = self.builder.get_object("service_list_headerbar")  # type: Gtk.HeaderBar
        self.panel_headerbar = self.builder.get_object("panel_headerbar")           # type: Gtk.HeaderBar
        self.placeholder_panel = self.builder.get_object("panel_placeholder")       # type: Gtk.Widget
        self.sidebar_box = self.builder.get_object("sidebar_box")                   # type: Gtk.Box
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

        self.window.connect("delete-event", Gtk.main_quit)
        self.window.set_title(WINDOW_TITLE)
        self.window.show_all()

        self.services = list()  # type: List[Service]
        self.current_service = None  # type: Service

        self.service_list = ServiceList(self)
        self.sidebar_box.add(self.service_list.box)

        SystemBus().watch_name("org.boum.tails.OnionKit",
                               flags=Gio.BusNameWatcherFlags.AUTO_START,
                               name_appeared=self.on_onionkit_appeared,
                               name_vanished=self.on_onionkit_vanished)

    def on_onionkit_appeared(self, owner):
        logger.debug("OnionKit bus available")
        # Ensure the onionkit_not_running panel is removed
        for child in self.stack.get_children():
            self.stack.remove(child)

        # It would be cleaner to always reload the services here, but unsubscribing from the PropertiesChanged signal
        # sometimes failed during cleanup in on_onionkit_vanished(), so the workaround is to not clean up the services
        # and reuse them with the new bus instead.
        if not self.services:
            self.services = [Service(self, service) for service in services.objects]
        else:
            for service in self.services:
                service.refresh_cache()

        logger.debug("Adding installed services")
        for service in [service for service in self.services if service.Status != Status.NOT_INSTALLED]:
            self.service_list.add(service)

        if len(self.service_list) == 0:
            self.show_placeholder_panel()

        # For debugging memory usage
        # from psutil import Process
        # logger.error("Memory used: %s", Process().memory_info().rss)

    def on_onionkit_vanished(self):
        logger.warning("OnionKit bus not available")
        # Remove all panels
        for child in self.stack.get_children():
            self.stack.remove(child)

        # Remove services from service list
        self.service_list.remove_all()

        self.current_service = None

        # XXX: There is a memory leak here. When onionkit is stopped and
        # started again, ~ 500 KiB more are used than before.

        self.show_onionkit_not_running_panel()

    def activate_panel(self, panel: "ServicePanel"):
        logger.debug("Activating panel of service %r", panel.service.Name)
        service = panel.service
        if self.current_service == service:
            return

        self.stack.add_named(panel.box, service.Name)
        self.stack.set_visible_child_name(service.Name)
        self.panel_headerbar.set_title(service.NameForDisplay)
        self.sidebar_headerbar.show()
        self.sidebar_box.show()
        panel.show()

        if self.current_service:
            self.stack.remove(self.current_service.panel.box)
        else:
            self.stack.remove(self.placeholder_panel)

        self.current_service = service

    def show_placeholder_panel(self):
        self.sidebar_box.hide()
        self.sidebar_headerbar.hide()
        self.panel_headerbar.set_title("")

        for child in self.stack.get_children():
            self.stack.remove(child)
        self.stack.add_named(self.placeholder_panel, "placeholder")
        self.stack.set_visible_child_name("placeholder")

    def show_onionkit_not_running_panel(self):
        self.sidebar_box.hide()
        self.sidebar_headerbar.hide()
        self.panel_headerbar.set_title("")
        label = Gtk.Label(
            label='Error: onionkit is not running.\n'
                  'You can check the output of the following commands to investigate:\n\n'
                  '<span font_desc="mono">sudo systemctl status onionkit</span>\n'
                  '<span font_desc="mono">sudo journalctl $(sudo which onionkitd)</span>\n',
            wrap=True,
            use_markup=True,
            selectable=True
        )
        alignment = Gtk.Alignment(yscale=0)
        alignment.add(label)
        alignment.show_all()

        for child in self.stack.get_children():
            self.stack.remove(child)
        self.stack.add_named(alignment, "onionkit_not_running")
        self.stack.set_visible_child_name("onionkit_not_running")

    def all_services_removed(self):
        self.current_service = None
        self.show_placeholder_panel()

    def on_window_destroy(self, obj, data=None):
        Gtk.main_quit()

    def on_close_clicked(self, button):
        Gtk.main_quit()

    def on_placeholder_add_service_button_clicked(self, button, data=None):
        ServiceChooserDialog(self).run()

    def on_placeholder_label_activate_link(self, label, uri):
        logger.debug("Opening documentation")
        subprocess.Popen(["sudo", "-u", TAILS_USER, "xdg-open", uri])
        return True

    def disable_main_window(self):
        self.window.set_sensitive(False)

    def enable_main_window(self):
        self.window.set_sensitive(True)
