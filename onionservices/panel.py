from collections import OrderedDict
from logging import getLogger
import threading
import subprocess

from gi.repository import Gtk

from onionservices import _
from onionservices import util

from onionservices import action_widgets
from onionservices import option_rows
from onionservices.option import Option
from onionservices.option_row import OptionRow, BooleanOptionRow, StringOptionRow, IntOptionRow
from onionservices.option_group_box import GroupBox
from onionservices.service_status import Status

from onionservices.config import APP_NAME, PANEL_UI_FILE, TAILS_USER

# Only required for type hints
from typing import TYPE_CHECKING, List
if TYPE_CHECKING:
    from onionservices.service import Service
    from onionservices.action_widget import ActionWidget


logger = getLogger(__name__)


class ServicePanel(object):

    def __init__(self, service: "Service"):
        logger.debug("Instantiating panel for service %r", service.Name)
        self.service = service  # type: "Service"
        self.gui = service.gui  # type: "MainWindow"
        self.builder = Gtk.Builder()
        self.builder.set_translation_domain(APP_NAME)
        self.builder.add_from_file(PANEL_UI_FILE)
        self.builder.connect_signals(self)
        self.box = self.builder.get_object("box")                                            # type: Gtk.Container
        self.options_box = self.builder.get_object("options_box")                            # type: Gtk.Box
        self.switch = self.builder.get_object("switch_service_start_stop")                   # type: Gtk.Switch
        self.status_label = self.builder.get_object("status_label")                          # type: Gtk.Label
        self.status_box = self.builder.get_object("status_box")                              # type: Gtk.Box
        self.outer_status_box = self.builder.get_object("outer_status_box")                  # type: Gtk.Box
        self.spinner = self.builder.get_object("spinner")                                    # type: Gtk.Spinner
        self.error_image = self.builder.get_object("image_error")                            # type: Gtk.Image
        self.transaction_status_label = self.builder.get_object("transaction_status_value")  # type: Gtk.Label
        self.transaction_progressbar = self.builder.get_object("transaction_progressbar")    # type: Gtk.ProgressBar
        self.documentation_label = self.builder.get_object("documentation_label")            # type: Gtk.Label
        self.documentation_label.set_markup('<a href="%s">Learn more</a>' % self.service.DocumentationURL)
        self.documentation_label.set_tooltip_text("Learn more about %s" % self.service.NameForDisplay)
        self.autostart_comment_label = self.builder.get_object("label_autostart_comment")    # type: Gtk.Label
        self.description_label = self.builder.get_object("label_description")                # type: Gtk.Label
        self.description_label.set_text(self.service.DescriptionLong)
        icon = self.builder.get_object("image_service_icon")                                 # type: Gtk.Image
        __, icon_size = icon.get_icon_name()
        icon.set_from_icon_name(self.service.Icon, icon_size)

        self.action_widgets = OrderedDict()  # type: OrderedDict[str, ActionWidget]
        self.option_rows = OrderedDict()     # type: OrderedDict[str, OptionRow]
        self.group_boxes = OrderedDict()    # type: OrderedDict[str, GroupBox]
        self.option_groups = list()
        self.active_status_widget = None

        if self.service.IsInstalled:
            self.populate_options()

    @property
    def is_active(self):
        return self.gui.current_service == self.service

    def populate_options(self):
        """For each option of this service, add a row to the panel"""
        logger.info("Adding options of service %r to the panel", self.service.Name)
        self.option_groups = [option.Group for option in self.service.options.values() if option.Group]
        self.add_group_boxes()
        self.add_action_widgets()
        self.add_option_rows()
        self.show_options()

    def add_action_widgets(self):
        for name in action_widgets.classes:
            logger.debug("Adding widget %r", name)
            action_widget = action_widgets.classes[name](self)
            action_widget.update()
            self.action_widgets[name] = action_widget

    def add_option_rows(self):
        for option in self.service.options.values():
            row = self.create_option_row(option)
            row.update()
            option.row = row
            self.option_rows[option.Name] = row
            logger.debug("Inserting option row %r to group %r", option.Name, option.Group)
            self.group_boxes[option.Group].add_row(row)

    def create_option_row(self, option: Option) -> OptionRow:
        logger.debug("Creating option row for option %r", option.Name)
        # Check if this option has a special class defined in option_rows/
        if option.Name in option_rows.classes:
            logger.debug("Using option row from module for option %r", option.Name)
            return option_rows.classes[option.Name](self, option)

        # Create a generic option based on the type of the option value
        if isinstance(option.Value, str):
            return StringOptionRow(self, option)
        # It is important that we test bool before int, because isinstance(bool(), int) is True
        if isinstance(option.Value, bool):
            return BooleanOptionRow(self, option)
        if isinstance(option.Value, int):
            return IntOptionRow(self, option)

        raise TypeError("option %r has unspported type %r", type(option.Value))

    def add_group_boxes(self):
        logger.debug("Adding group boxes")
        logger.debug("Groups: %r", self.option_groups)
        for group in self.option_groups:
            if group not in self.group_boxes:
                self.add_group_box(group)

    def add_group_box(self, group: str):
        logger.debug("Adding box for group %r", group)
        self.group_boxes[group] = GroupBox(self, group)

    def on_status_changed(self):
        if self.service.Status == Status.UNINSTALLING:
            self.hide_options()

        self.update_status_label()
        self.update_status_widget()
        self.update_switch()

    def update_status_label(self):
        self.status_label.set_text(self.service.Status)
        self.status_label.show()

    def update_status_widget(self):
        status_widget = None
        if self.service.Status in (Status.STARTING, Status.STOPPING, Status.INSTALLING, Status.UNINSTALLING, Status.PUBLISHING):
            status_widget = self.spinner
        if self.service.Status in (Status.ERROR, Status.ERROR_TOR_IS_NOT_RUNNING, Status.STOPPED_UNEXPECTEDLY):
            status_widget = self.error_image

        if status_widget == self.active_status_widget:
            return

        if self.active_status_widget:
            self.status_box.remove(self.active_status_widget)

        if status_widget:
            self.status_box.add(status_widget)

        self.active_status_widget = status_widget

    def update_switch(self):
        # Set switch to "on" or "off" state
        if self.service.Status in (Status.RUNNING, Status.STARTING):
            self.switch.set_active(True)
        else:
            self.switch.set_active(False)

        # Set switch underyling state (i.e. grayed out or glowing blue)
        if self.service.Status == Status.RUNNING:
            self.switch.set_state(True)
        else:
            self.switch.set_state(False)

        # Set switch sensibility
        if self.service.Status in (Status.RUNNING, Status.STOPPED, Status.STOPPED_UNEXPECTEDLY, Status.ERROR):
            self.switch.set_sensitive(True)
        else:
            self.switch.set_sensitive(False)

    def update_transaction_status_label(self):
        if self.service.TransactionStatus:
            self.transaction_status_label.set_text(self.service.TransactionStatus)
            self.transaction_status_label.show()
        else:
            self.transaction_status_label.hide()

    def update_transaction_progressbar(self):
        if self.service.TransactionProgress == 101:
            self.transaction_progressbar.hide()
            self.outer_status_box.set_margin_bottom(16)
        else:
            self.transaction_progressbar.set_fraction(self.service.TransactionProgress / 100)
            self.transaction_progressbar.show()
            self.outer_status_box.set_margin_bottom(6)

    def show(self):
        logger.log(5, "Showing config panel of service %r", self.service.Name)
        if self.service.Status == Status.UNINSTALLING:
            self.hide_options()
        else:
            self.show_options()

        self.box.show_all()

    def show_options(self):
        self.options_box.set_no_show_all(False)
        self.options_box.show_all()

    def hide_options(self):
        self.options_box.hide()
        self.options_box.set_no_show_all(True)

    def set_switch_status(self, status):
        logger.debug("Setting switch status to %r", status)
        self.switch.set_active(status)

    def update_switch_status(self):
        if self.service.Status in [Status.STOPPED, Status.STOPPING]:
            self.set_switch_status(False)
        else:
            self.set_switch_status(True)

    def on_switch_state_set(self, switch, state):
        logger.log(5, "Service switch state set to: %r", state)
        if not state:
            switch.set_state(False)
        # We have to return True here to prevent the default handler from running, which would
        # sync the "state" property with the "active" property. We don't want this, because we
        # want to set "state" only to True when the service is actually "Running".
        # See https://developer.gnome.org/gtk3/stable/GtkSwitch.html#GtkSwitch-state-set
        return True

    def on_switch_active(self, switch, data):
        status = switch.get_active()
        logger.log(5, "Service switch active set to: %r", status)
        util.run_threaded_when_idle(self.on_switch_clicked, status)

    def on_switch_clicked(self, status):
        # self.update_onion_address_widget()
        # self.set_connection_info_sensitivity()

        if status and self.service.Status in (Status.STOPPED, Status.STOPPED_UNEXPECTEDLY):
            util.run_threaded(self.service.start)
            self.show()
        elif not status and self.service.Status == Status.RUNNING:
            util.run_threaded(self.service.stop)

    def on_button_help_clicked(self, button):
        logger.debug("Opening documentation for service %r", self.service.Name)
        subprocess.Popen(["sudo", "-u", TAILS_USER, "xdg-open", self.service.DocumentationURL])
        return True
