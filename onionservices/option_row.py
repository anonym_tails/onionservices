from logging import getLogger

from gi.repository import Gtk

from onionservices.action_widget import ClickableRow
from onionservices.edit_dialog import EditDialog

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.option import Option
    from onionservices.panel import ServicePanel

logger = getLogger(__name__)


class OptionRow(object):
    def __init__(self, panel: "ServicePanel", option: "Option"):
        self.panel = panel
        self.option = option
        self.widget = None  # type: Gtk.ListBoxRow

    def update(self):
        """Adjust the option row according to the option's current value"""
        pass


class ClickableOptionRow(OptionRow, ClickableRow):
    def __init__(self, panel: "ServicePanel", option: "Option"):
        OptionRow.__init__(self, panel, option)
        ClickableRow.__init__(self, panel, option.NameForDisplay)


class BooleanOptionRow(ClickableOptionRow):
    def __init__(self, panel, option):
        super().__init__(panel, option)
        self.switch = Gtk.Switch()
        self.switch.connect("state-set", self.on_switch_state_set)
        self.box.add(self.switch)

        # Adjust margin, because the button is higher than other widgets
        self.box.set_margin_top(13)
        self.box.set_margin_bottom(13)

    def update(self):
        self.switch.set_active(self.option.Value)

    def on_activated(self):
        # We don't want to do anything if the listboxrow is activated, but only if the switch is set
        pass

    def on_switch_state_set(self, switch, state):
        self.option.Value = state
        self.update()


class StringOptionRow(ClickableOptionRow):
    def __init__(self, panel, option):
        logger.debug("Creating StringOptionRow")
        super().__init__(panel, option)

        # Add a value label
        self.label = Gtk.Label()
        self.box.add(self.label)

    def update(self):
        if not self.option.Value:
            self.label.set_markup('<span style="italic">(Empty)</span>')
            self.label.set_sensitive(False)
            return
        self.label.set_sensitive(True)

        if self.option.Masked:
            self.label.set_text("•" * len(self.option.Value))
        else:
            self.label.set_label(self.option.Value)

    def on_activated(self):
        if not self.option.Writable:
            return

        dialog = EditDialog(self.panel.gui.window, self.option.NameForDisplay, self.option.Value,
                            masked=self.option.Masked)
        if dialog.run() != Gtk.ResponseType.APPLY:
            dialog.destroy()
            return

        self.option.Value = dialog.entry.get_text()
        dialog.destroy()


class IntOptionRow(ClickableOptionRow):
    def __init__(self, panel, option):
        logger.debug("Creating IntOptionRow")
        super().__init__(panel, option)

        # Add a value label
        self.label = Gtk.Label()
        self.box.add(self.label)

    def update(self):
        self.label.set_label(str(self.option.Value))

    def on_activated(self):
        if not self.option.Writable:
            return

        dialog = EditDialog(self.panel.gui.window, self.option.NameForDisplay, self.option.Value,
                            entry_changed_callback=self.on_entry_changed)

        if dialog.run() != Gtk.ResponseType.APPLY:
            dialog.destroy()
            return

        self.option.Value = int(dialog.entry.get_text())
        dialog.destroy()

    def on_entry_changed(self, entry, dialog):
        text = entry.get_text()
        if not text:
            dialog.apply_button.set_sensitive(False)
        elif not text.isdigit():
            entry.get_style_context().add_class("error")
            dialog.apply_button.set_sensitive(False)
        else:
            entry.get_style_context().remove_class("error")
            dialog.apply_button.set_sensitive(True)
