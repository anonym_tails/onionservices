# Translation stuff

import os
import gettext

from .config import ONIONKIT_BUS, ONIONKIT_SERVICES_PATH, APP_NAME


if os.path.exists('po/locale'):
    translation = gettext.translation(APP_NAME, 'po/locale', fallback=True)
else:
    translation = gettext.translation(APP_NAME, '/usr/share/locale', fallback=True)

_ = translation.gettext


# Services class to access dbus objects of OnionKit services

from pydbus import SystemBus
from typing import TYPE_CHECKING, List, Dict

if TYPE_CHECKING:
    from onionservices.service import OnionService


class Services(object):

    @property
    def names(self) -> List[str]:
        return list(self.objects_dict.keys())

    @property
    def objects(self) -> List["OnionService"]:
        return list(self.objects_dict.values())

    @property
    def objects_dict(self) -> Dict[str, "OnionService"]:
        bus = SystemBus()
        paths = bus.get(ONIONKIT_BUS).GetServices()
        return {os.path.basename(path): path for path in paths}

    def get_service(self, name: str) -> "OnionService":
        return self.objects_dict[name]


services = Services()
