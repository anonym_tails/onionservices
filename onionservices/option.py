from logging import getLogger

from gi.repository import GLib
from pydbus import SystemBus

from onionservices.config import ONIONKIT_BUS

# Only required for type hints
from typing import TYPE_CHECKING, Dict, Any
if TYPE_CHECKING:
    from onionservices.service import Service
    from onionservices.option_row import OptionRow

logger = getLogger()


class Option(object):
    """Option wrapper extended by functionality used in the GUI"""

    def __init__(self, service: "Service", path):
        self.service = service
        self.option = SystemBus().get(ONIONKIT_BUS, path)
        logger.debug("Initializing option %r", path)

        # Subscribe to the PropertiesChanged signal
        self.subscription = self.option.PropertiesChanged.connect(self.on_properties_changed)

        # Cache the DBus object's properties
        self._value = self.option.Value
        self.Name = self.option.Name
        self.NameForDisplay = self.option.NameForDisplay
        self.Description = self.option.Description
        self.Group = self.option.Group
        self.Masked = self.option.Masked
        self.Writable = self.option.Writable

        self.row = None  # type: OptionRow

    @property
    def Value(self):
        return self._value

    @Value.setter
    def Value(self, value):
        """Sets both the cached value and onionkit's option value"""
        self._value = value
        if self.option.Value != value:
            self.option.Value = self._option_to_variant(value)

    def _option_to_variant(self, value: Any) -> GLib.Variant:
        if isinstance(self.Value, str):
            return GLib.Variant("s", str(value))
        # It is important that we test bool before int, because isinstance(bool(), int) is True
        if isinstance(self.Value, bool):
            return GLib.Variant("b", bool(value))
        if isinstance(self.Value, int):
            return GLib.Variant("i", int(value))
        raise TypeError("Option %r has unsupported type %r" % (self.Name, type(value)))

    def reload(self):
        self.option.Reload()

    def on_properties_changed(self, properties: Dict[str, Any]):
        logger.debug("Received PropertiesChanged signal for option %r", self.Name)
        if "Value" in properties:
            self._value = properties["Value"]
            GLib.idle_add(self.on_value_updated)

    def on_value_updated(self):
        logger.debug("New value for '%s.%s': %r", self.service.Name, self.Name, self.Value)
        if self.row:
            self.row.update()

    def cleanup(self):
        self.subscription.disconnect()
