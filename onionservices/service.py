from logging import getLogger
import threading
from os import path
from collections import OrderedDict

from gi.repository import GLib, Gtk
from pydbus import SystemBus

from onionservices.panel import ServicePanel
from onionservices.option import Option

from onionservices.config import ONIONKIT_BUS, ONIONKIT_SERVICES_PATH

from onionservices.service_status import Status

# Only required for type hints
from typing import TYPE_CHECKING, List, Dict, Any
if TYPE_CHECKING:
    from onionservices.window import MainWindow
    from onionservices.service_list import ServiceList, ServiceRow
    from onionservices.service_chooser import ServiceChooserRow

logger = getLogger()


class Service(object):
    """Service wrapper extended by functionality used in the GUI"""

    def __init__(self, gui: "MainWindow", service_name: str):
        self.gui = gui
        self.service_list = self.gui.service_list  # type: ServiceList
        self.service = SystemBus().get(ONIONKIT_BUS, path.join(ONIONKIT_SERVICES_PATH, service_name))
        # This lock is used to prevent applying options while the service is starting
        self.lock = threading.Lock()

        # Subscribe to the PropertiesChanged signal
        # https://github.com/LEW21/pydbus/blob/master/doc/tutorial.rst#object-api
        self.subscription = self.service.PropertiesChanged.connect(self.on_properties_changed)

        self.Options = None                      # type: List[str]
        self.Name = None                         # type: str
        self.NameForDisplay = None               # type: str
        self.Description = None                  # type: str
        self.DescriptionLong = None              # type: str
        self.DocumentationURL = None             # type: str
        self.Icon = None                         # type: str
        self.ClientApplication = None            # type: str
        self.ClientApplicationForDisplay = None  # type: str
        self.Address = None                      # type: str
        self.ConnectionInfo = None               # type: str
        self.Status = None                       # type: str
        self.TransactionStatus = None            # type: str
        self.TransactionProgress = None          # type: int
        self.IsInstalled = None                  # type: bool

        self.refresh_cache()

    def refresh_cache(self):
        # Cache the DBus object's properties
        # We keep the CamelCase as a hint that this is the value of the DBus property
        self.Options = self.service.Options
        self.Name = self.service.Name
        self.NameForDisplay = self.service.NameForDisplay
        self.Description = self.service.Description
        self.DescriptionLong = self.service.DescriptionLong
        self.DocumentationURL = self.service.DocumentationURL
        self.Icon = self.service.Icon
        self.ClientApplication = self.service.ClientApplication
        self.ClientApplicationForDisplay = self.service.ClientApplicationForDisplay
        self.Address = self.service.Address
        self.ConnectionInfo = self.service.ConnectionInfo
        self.Status = self.service.Status
        self.TransactionStatus = self.service.TransactionStatus
        self.TransactionProgress = self.service.TransactionProgress
        self.IsInstalled = self.service.IsInstalled

        # Initialize service's options
        self.options = self._get_options()

        self.panel = None  # type: ServicePanel

        # ServiceRow in ServiceList
        self.row = None  # type: ServiceRow

        # row in ServiceChooser
        self.chooser_row = None  # type: ServiceChooserRow

        # Apply current status
        GLib.idle_add(self.on_status_changed, self.service.Status)
        GLib.idle_add(self.on_transaction_status_changed, self.service.TransactionStatus)
        GLib.idle_add(self.on_transaction_progress_changed, self.service.TransactionProgress)

    def _get_options(self) -> Dict[str, Option]:
        return OrderedDict((
            [option.Name, option] for option in [Option(self, option_path) for option_path in self.Options]
        ))

    def install(self):
        self.service.Install()

    def uninstall(self):
        self.service.Uninstall()

        # Remove service from the service list if we are uninstalled and for
        # some reason still in the service list
        if not self.IsInstalled and self in self.service_list:
            logger.warning("Service %r is still in the service list. Removing.", self.Name)
            try:
                self.service_list.remove(self)
            except ValueError:
                pass

    def start(self):
        with self.lock:
            self.service.Start()

    def stop(self):
        self.service.Stop()

    def regenerate_address(self):
        self.service.RegenerateAddress()

    def on_properties_changed(self, properties: Dict[str, Any]):
        for name, value in properties.items():
            if not hasattr(self, name):
                raise AttributeError("type object %r has no attribute %r" % (self, name))
            setattr(self, name, value)

        if "Status" in properties:
            GLib.idle_add(self.on_status_changed, properties["Status"])
        if "TransactionStatus" in properties:
            GLib.idle_add(self.on_transaction_status_changed, properties["TransactionStatus"])
        if "TransactionProgress" in properties:
            GLib.idle_add(self.on_transaction_progress_changed, properties["TransactionProgress"])
        if "IsInstalled" in properties:
            GLib.idle_add(self.on_is_installed_changed)
        if "Address" in properties:
            GLib.idle_add(self.on_address_changed)
        if "ConnectionInfo" in properties:
            GLib.idle_add(self.on_connection_info_changed)
        if "Options" in properties:
            GLib.idle_add(self.on_options_changed)

    def on_status_changed(self, status):
        logger.info("New status for service %r: %r", self.Name, status)

        if status == Status.INSTALLING and self not in self.service_list:
            self.service_list.add(self)

        if self.panel:
            self.panel.on_status_changed()

        if self.row:
            self.row.update()

    def on_transaction_status_changed(self, transaction_status):
        logger.debug("New transaction status for service %r: %r", self.Name, transaction_status)
        self.TransactionProgress = 101
        if self.panel:
            self.panel.update_transaction_status_label()
            self.panel.update_transaction_progressbar()

    def on_transaction_progress_changed(self, transaction_progress):
        logger.debug("New transaction progress for service %r: %r", self.Name, transaction_progress)
        if self.panel:
            self.panel.update_transaction_progressbar()

    def on_is_installed_changed(self):
        logger.info("New installation status for service %r: %r", self.Name, self.IsInstalled)
        if self.IsInstalled:
            self.on_service_installed()
        elif self in self.service_list:
            self.on_service_uninstalled()

    def on_options_changed(self):
        self.options = self._get_options()

    def on_address_changed(self):
        """Update the address widget if it is populated"""
        logger.debug("Address updated")
        if self.panel and "Address" in self.panel.action_widgets:
            self.panel.action_widgets["Address"].update()

    def on_connection_info_changed(self):
        """Update the invite button if it is populated"""
        logger.debug("ConnectionInfo updated")
        if self.panel and "InviteButton" in self.panel.action_widgets:
            self.panel.action_widgets["InviteButton"].update()

    def on_service_installed(self):
        if self.chooser_row:
            self.chooser_row.disable()
        self.panel.populate_options()

    def on_service_uninstalled(self):
        if self.chooser_row:
            self.chooser_row.enable()
        for option in self.options.values():
            option.cleanup()
        self.service_list.remove(self)
        self.panel = None

    def on_service_added(self):
        # Initialize config panel
        self.panel = ServicePanel(self)

    def on_service_selected(self):
        self.gui.activate_panel(self.panel)
