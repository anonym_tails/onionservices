import os
import glob
from collections import OrderedDict
from importlib.machinery import SourceFileLoader

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.action_widget import ActionWidget

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

classes = OrderedDict()  # type: OrderedDict[str, ActionWidget]

paths = glob.glob(os.path.join(SCRIPT_DIR, "**", "*.py"), recursive=True)
paths.remove(os.path.join(SCRIPT_DIR, "__init__.py"))
paths.sort()

for path in paths:
    loader = SourceFileLoader(os.path.basename(path), path)
    module_ = loader.load_module()
    class_name = module_.address_widget_class.__name__
    action_widget_name = class_name[:-3] if class_name.endswith("Row") else class_name
    classes[action_widget_name] = module_.address_widget_class
