from logging import getLogger
import time

from gi.repository import Gtk, Gdk

from onionservices import _
from onionservices import util
from onionservices.action_widget import ClickableRow
from onionservices.service_status import Status
from onionservices.warning_dialog import WarningDialog

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.panel import ServicePanel
    from onionservices.service import Service

logger = getLogger(__name__)


class AddressRow(ClickableRow):
    def __init__(self, panel: "ServicePanel"):
        super().__init__(panel, _("Address"))

        # Add the address value label
        self.label = Gtk.Label()
        self.box.add(self.label)

        # Add address row to connection listbox
        self.panel.group_boxes[_("Connection")].add_row(self)

    def update(self):
        if self.panel.service.Address:
            self.label.set_text(self.panel.service.Address)
        else:
            logger.warning("Service %r doesn't have an address. This should never happen", self.panel.service)
            self.label.set_text("")

    def on_activated(self):
        logger.debug("Address row clicked")
        dialog = AddressDialog(self.panel.gui.window, self.panel.service)
        dialog.run()


class AddressDialog(object):
    def __init__(self, parent: Gtk.Window, service: "Service"):
        self.service = service
        self.selected = False

        self.dialog = Gtk.Dialog(
            parent=parent,
            title=_("Address"),
            modal=True,
            use_header_bar=True,
            resizable=False,
        )

        label = Gtk.Label(
            label=_("This is the onion address others can use to connect to this service."),
            visible=True,
            xalign=0,
            wrap=True,
            max_width_chars=50
        )
        label.get_style_context().add_class("dim-label")

        self.entry = Gtk.Entry(
            text=self.service.Address,
            visible=True,
            editable=False,
            can_focus=False,
        )
        self.entry.get_style_context().add_class("read-only")
        self.entry.connect("button-press-event", self.select_text)
        self.dialog.connect("key-press-event", self.on_key_pressed)

        button = Gtk.Button(
            label=_("Generate new address..."),
            visible=True,
            halign=Gtk.Align.START,
        )
        button.connect("clicked", self.on_generate_new_address_clicked)

        content_box = self.dialog.get_content_area()
        content_box.set_margin_top(18)
        content_box.set_margin_bottom(18)
        content_box.set_margin_left(18)
        content_box.set_margin_right(18)
        content_box.set_spacing(24)
        content_box.add(label)
        content_box.add(self.entry)
        content_box.add(button)

    def run(self):
        self.dialog.run()
        self.dialog.destroy()

    def on_generate_new_address_clicked(self, button):
        text = _("This will irrevocably delete this service's onion address and generate a new one.")
        if self.service.Status in (Status.RUNNING, Status.STARTING, Status.PUBLISHING):
            text += "\n\n" + _("The service will be restarted during the address generation.")

        warning = WarningDialog(
            parent=self.dialog,
            title=_("Delete and generate new address?"),
            text=text,
            button_title=_("Delete Address")
        )

        if warning.run() == Gtk.ResponseType.OK:
            old_address = self.service.Address
            self.service.regenerate_address()

            # Wait for the new address to be available
            while self.service.Address == old_address:
                util.process_mainloop_events()
                time.sleep(0.1)
            # Set entry to the new address
            self.entry.set_text(self.service.Address)

    def select_text(self, entry, event):
        """Select the whole address the first time the entry is left clicked"""
        if event.button == 1 and not self.selected:
            entry.select_region(0, -1)
            self.selected = True
            return True

    def on_key_pressed(self, widget, event):
        # Copy the selection of the entry to the clipboard if Ctrl-c is pressed
        if event.keyval == Gdk.KEY_c and event.state & Gdk.ModifierType.CONTROL_MASK:
            self.entry.copy_clipboard()


address_widget_class = AddressRow
