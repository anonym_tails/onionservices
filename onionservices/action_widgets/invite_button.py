from logging import getLogger

from gi.repository import Gtk

from onionservices import _
from onionservices.action_widget import ActionWidget

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.panel import ServicePanel, Service

logger = getLogger(__name__)


class InviteButton(ActionWidget):
    def __init__(self, panel: "ServicePanel"):
        super().__init__(panel)

        # Create button
        button = Gtk.Button()
        button.set_label(_("_Connect & Invite Others"))
        button.set_use_underline(True)
        button.set_receives_default(True)
        button.connect("clicked", self.on_activated)
        self.widget = button

        # Add button to connection label box
        group_box = self.panel.group_boxes[_("Connection")]
        group_box.label_box.add(self.widget)

        # Adjust position of connection label
        group_box.label.set_margin_bottom(3)
        group_box.label_box.set_margin_bottom(9)
        group_box.label.set_valign(Gtk.Align.END)

        # Make the button sensitive if ConnectionInfo is available
        self.update()

    def update(self):
        if self.panel.service.ConnectionInfo:
            self.widget.set_sensitive(True)
        else:
            self.widget.set_sensitive(False)

    def on_activated(self, widget):
        dialog = InviteDialog(self.panel.gui.window, self.panel.service)
        dialog.run()


class InviteDialog(object):
    def __init__(self, parent: Gtk.Window, service: "Service"):
        self.selected = False

        self.dialog = Gtk.Dialog(
            parent=parent,
            title=_("Connect & Invite Others"),
            modal=True,
            use_header_bar=True,
            resizable=False,
        )

        # XXX: Change URL to documentation link
        label = Gtk.Label(
            label=_("Below is a summary of the information that is required to connect to this service. "
                    "Share it via a secure channel to allow others connecting to this service.\n\n"
                    "This information can be used to configure the client application manually, "
                    "or it can be passed to <a href='https://gitlab.com/segfault3/onionclient'>"
                    "OnionClient</a> to automatically configure the client application."),
            use_markup=True,
            xalign=0,
            wrap=True,
            max_width_chars=50
        )
        label.get_style_context().add_class("dim-label")

        textbuffer = Gtk.TextBuffer(text=service.ConnectionInfo)

        textview = Gtk.TextView(
            buffer=textbuffer,
            editable=False,
            top_margin=10,
            bottom_margin=10,
            left_margin=10,
            right_margin=10,
        )
        textview.connect("button-press-event", self.select_text)

        frame = Gtk.Frame()
        frame.add(textview)

        content_box = self.dialog.get_content_area()
        content_box.set_margin_top(18)
        content_box.set_margin_bottom(18)
        content_box.set_margin_left(18)
        content_box.set_margin_right(18)
        content_box.set_spacing(24)
        content_box.add(label)
        content_box.add(frame)
        content_box.show_all()

    def select_text(self, textview: Gtk.TextView, event):
        """Select the whole text the first time the textview is left clicked"""
        if event.button == 1 and not self.selected:
            buffer = textview.get_buffer()  # type: Gtk.TextBuffer
            buffer.select_range(buffer.get_start_iter(), buffer.get_end_iter())
            self.selected = True
            return True

    def run(self):
        self.dialog.run()
        self.dialog.destroy()


address_widget_class = InviteButton
