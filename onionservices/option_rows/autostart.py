from logging import getLogger

from gi.repository import Gtk

from onionservices import _
from onionservices.option_row import BooleanOptionRow, ClickableOptionRow

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.service import Service

logger = getLogger(__name__)


class AutostartRow(ClickableOptionRow):

    def __init__(self, panel, option):
        super().__init__(panel, option)

        # Add comment label
        self.comment_label = Gtk.Label(
            label='<span style="italic">(Requires Persistence)</span>',
            use_markup=True,
            no_show_all=True,
            halign=Gtk.Align.START,
            hexpand=True
        )
        self.box.add(self.comment_label)

        # Add value label
        self.label = Gtk.Label()
        self.box.add(self.label)

    def update(self):
        if self.option.Value:
            self.label.set_text(_("On"))
        else:
            self.label.set_text(_("Off"))

        # The Autostart option requires that persistence is enabled. If it is not,
        # we make this widget insensitive and display a comment.
        if "Persistence" in self.panel.service.options and self.panel.service.options["Persistence"].Value:
            self.make_sensitive()
        else:
            self.make_insensitive()

    def make_sensitive(self):
        self.comment_label.hide()
        self.title.set_hexpand(True)
        self.widget.set_sensitive(True)

    def make_insensitive(self):
        logger.info("Making autostart option insensitive because persistence is not active")
        self.comment_label.show()
        self.title.set_hexpand(False)
        self.widget.set_sensitive(False)

    def on_activated(self):
        dialog = AutostartDialog(self.panel.gui.window, self.panel.service)
        dialog.run()


class AutostartDialog(object):
    def __init__(self, parent: Gtk.Window, service: "Service"):
        self.service = service
        self.selected = False

        self.dialog = Gtk.Dialog(
            parent=parent,
            title=_("Autostart"),
            modal=True,
            use_header_bar=True,
            resizable=False,
        )

        label = Gtk.Label(
            label=_("Enable autostart to automatically start this service after booting the system.\n\n"
                    "This requires that the service is persistent."),
            visible=True,
            xalign=0,
            wrap=True,
            max_width_chars=50
        )
        label.get_style_context().add_class("dim-label")

        row = BooleanOptionRow(self.service.panel, self.service.options["Autostart"])
        row.switch.set_active(row.option.Value)

        listbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
        listbox.add(row.widget)
        listbox.show_all()

        content_box = self.dialog.get_content_area()
        content_box.set_margin_top(18)
        content_box.set_margin_bottom(18)
        content_box.set_margin_left(18)
        content_box.set_margin_right(18)
        content_box.set_spacing(24)
        content_box.add(label)
        content_box.add(listbox)

    def run(self):
        self.dialog.run()
        self.dialog.destroy()


option_row_class = AutostartRow
