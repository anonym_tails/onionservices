from gi.repository import Gtk, Gdk, Pango

from onionservices import _
from onionservices.option_row import StringOptionRow

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.service import Option


class TLSFingerprintRow(StringOptionRow):

    def on_activated(self):
        dialog = FingerprintDialog(self.panel.gui.window, self.option)
        dialog.run()


class FingerprintDialog(object):
    def __init__(self, parent: Gtk.Window, option: "Option"):
        self.selected = False

        self.dialog = Gtk.Dialog(
            parent=parent,
            title=_("TLS Fingerprint"),
            modal=True,
            use_header_bar=True,
            resizable=False,
        )

        if not option.Value:
            label = Gtk.Label(
                label=_("The TLS certificate is not available before you start the service."),
                visible=True,
            )
            content_box = self.dialog.get_content_area()  # type: Gtk.Container
            content_box.set_margin_top(18)
            content_box.set_margin_bottom(18)
            content_box.set_margin_left(18)
            content_box.set_margin_right(18)
            content_box.add(label)
            return

        label = Gtk.Label(
            label=_("This is the fingerprint of the self-signed TLS certificate used by this service. "
                    "Users should verify that this matches the certificate presented by the service "
                    "when they are connecting."),
            visible=True,
            xalign=0,
            wrap=True,
            max_width_chars=1
        )
        label.get_style_context().add_class("dim-label")

        self.entry = Gtk.Entry(
            text=option.Value,
            visible=True,
            editable=False,
            can_focus=False,
        )
        self.entry.modify_font(Pango.FontDescription("monospace"))
        self.entry.set_width_chars(self.entry.get_text_length())
        self.entry.get_style_context().add_class("read-only")
        self.entry.connect("button-press-event", self.select_text)
        self.dialog.connect("key-press-event", self.on_key_pressed)

        content_box = self.dialog.get_content_area()  # type: Gtk.Container
        content_box.set_margin_top(18)
        content_box.set_margin_bottom(18)
        content_box.set_margin_left(18)
        content_box.set_margin_right(18)
        content_box.set_spacing(24)
        content_box.pack_start(label, True, True, 0)
        content_box.add(self.entry)

    def run(self):
        self.dialog.run()
        self.dialog.destroy()

    def select_text(self, entry, event):
        """Select the whole address the first time the entry is left clicked"""
        if event.button == 1 and not self.selected:
            entry.select_region(0, -1)
            self.selected = True
            return True

    def on_key_pressed(self, widget, event):
        # Copy the selection of the entry to the clipboard if Ctrl-c is pressed
        if event.keyval == Gdk.KEY_c and event.state & Gdk.ModifierType.CONTROL_MASK:
            self.entry.copy_clipboard()


option_row_class = TLSFingerprintRow
