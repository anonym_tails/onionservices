import os
import glob
from collections import OrderedDict
from importlib.machinery import SourceFileLoader

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.option_row import OptionRow

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

classes = OrderedDict()  # type: OrderedDict[str, OptionRow]

search_string = os.path.join(SCRIPT_DIR, "**", "*.py")
print("search string: %r" % search_string)
paths = glob.glob(search_string, recursive=True)
paths.remove(os.path.join(SCRIPT_DIR, "__init__.py"))
print("paths:", paths)
paths.sort()

for path in paths:
    loader = SourceFileLoader(os.path.basename(path), path)
    module_ = loader.load_module()
    class_name = module_.option_row_class.__name__
    option_name = class_name[:-3] if class_name.endswith("Row") else class_name
    classes[option_name] = module_.option_row_class
