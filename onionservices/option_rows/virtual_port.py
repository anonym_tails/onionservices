from onionservices.option_row import IntOptionRow


class VirtualPortRow(IntOptionRow):
    def on_entry_changed(self, entry, dialog):
        text = entry.get_text()
        if not text:
            dialog.apply_button.set_sensitive(False)
        elif not text.isdigit() or not 0 < int(text) < 65536:
            entry.get_style_context().add_class("error")
            dialog.apply_button.set_sensitive(False)
        else:
            entry.get_style_context().remove_class("error")
            dialog.apply_button.set_sensitive(True)


option_row_class = VirtualPortRow
