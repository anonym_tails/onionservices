from gi.repository import Gtk

# Only required for type hints
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from onionservices.panel import ServicePanel


class ActionWidget(object):
    """A widget that is displayed in the panel and can perform an
    action on user interaction"""

    def __init__(self, panel: "ServicePanel"):
        self.panel = panel
        self.widget = None  # type: Gtk.Widget

    def update(self):
        pass

    def on_activated(self, *args):
        """Perform the action"""
        pass


class ClickableRow(ActionWidget):
    """An ActionWidget that consists of a listboxrow which displays a
    title and a value in labels, and can be clicked to perform an action"""

    def __init__(self, panel: "ServicePanel", title: str):
        super().__init__(panel)

        # Create widgets
        self.widget = Gtk.ListBoxRow()

        # Create the box that will be added to the row
        self.box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 0)
        self.box.set_margin_top(18)
        self.box.set_margin_bottom(18)
        self.box.set_margin_left(12)
        self.box.set_margin_right(12)

        # Create a title label
        self.title = Gtk.Label(title)
        self.title.set_halign(Gtk.Align.START)
        self.title.set_hexpand(True)
        self.title.set_margin_right(10)
        self.box.add(self.title)

        self.widget.add(self.box)

    def on_activated(self):
        pass
