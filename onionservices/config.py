from os import path
PACKAGE_PATH = path.dirname(path.realpath(__file__))

APP_NAME = "onionservices"
WINDOW_TITLE = "Onion Services"

TAILS_USER = "user"

ONIONKIT_BUS = "org.boum.tails.OnionKit"
ONIONKIT_BASE_PATH = "/org/boum/tails/OnionKit"
ONIONKIT_SERVICES_PATH = path.join(ONIONKIT_BASE_PATH, "services")

DATA_DIR = "/usr/share/%s/" % APP_NAME
UI_DIR = path.join(DATA_DIR, "ui")

ICON_DIR = path.join(DATA_DIR, "icons")
WINDOW_UI_FILE = path.join(UI_DIR, "window.ui")
PANEL_UI_FILE = path.join(UI_DIR, "panel.ui")
SERVICE_CHOOSER_UI_FILE = path.join(UI_DIR, "service_chooser.ui")
LOADING_WINDOW_UI_FILE = path.join(UI_DIR, "loading_window.ui")
SERVICE_LIST_UI_FILE = path.join(UI_DIR, "service_list.ui")
SERVICE_OPTION_UI_FILE = path.join(UI_DIR, "service_option.ui")
CONNECTION_INFO_UI_FILE = path.join(UI_DIR, "connection_info.ui")
QUESTION_DIALOG_UI_FILE = path.join(UI_DIR, "question_dialog.ui")
EDIT_DIALOG_UI_FILE = path.join(UI_DIR, "edit_dialog.ui")

