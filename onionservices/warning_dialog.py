from gi.repository import Gtk

from onionservices import _


class WarningDialog(object):

    def __init__(self, parent, title, text, button_title):
        self.dialog = Gtk.MessageDialog(
            parent=parent,
            type=Gtk.MessageType.WARNING,
            text=title,
            secondary_text=text
        )

        self.dialog.add_button(_("Cancel"), Gtk.ResponseType.CANCEL)
        self.dialog.add_button(button_title, Gtk.ResponseType.OK)
        self.dialog.set_default_response(False)

        button = self.dialog.get_widget_for_response(Gtk.ResponseType.OK)
        button.get_style_context().add_class("destructive-action")

    def run(self):
        response = self.dialog.run()
        self.dialog.destroy()
        return response
