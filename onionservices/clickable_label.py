class ClickableLabel(object):
    @property
    def clickable(self):
        return self._clickable

    @clickable.setter
    def clickable(self, value):
        if self._clickable == value:
            return
        if value:
            self._make_clickable()
        else:
            self._make_not_clickable()

    def __init__(self, container, button, button_box, label, image):
        self.container = container
        self.container.set_size_request(width=-1, height=34)
        self.button = button
        self.button_box = button_box
        self.label = label
        self.image = image
        self._clickable = True
        self._make_clickable()

    def _make_not_clickable(self):
        self.container.remove(self.button)
        self.button_box.remove(self.label)
        # self.value_widget.set_padding(0, 4)
        self.container.pack_start(self.label, expand=True, fill=True, padding=9)
        self.button_box.remove(self.image)
        self.container.pack_end(self.image, expand=False, fill=False, padding=9)
        self.label.set_selectable(True)
        self.image.set_visible(False)
        self.image.set_no_show_all(True)
        self._clickable = False

    def _make_clickable(self):
        if not self._clickable:
            self.container.remove(self.label)
            self.container.remove(self.image)
        for child in self.container.get_children():
            self.container.remove(child)
        for child in self.button_box.get_children():
            self.button_box.remove(child)
        self.button_box.pack_start(self.label, expand=True, fill=True, padding=0)
        self.button_box.pack_end(self.image, expand=False, fill=False, padding=0)
        self.container.pack_start(self.button, expand=True, fill=True, padding=0)
        self.label.set_selectable(False)
        self.image.set_visible(True)
        self.image.set_no_show_all(False)
        self._clickable = True
