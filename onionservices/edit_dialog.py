from gi.repository import Gtk, Gdk

from onionservices.config import APP_NAME, EDIT_DIALOG_UI_FILE


class EditDialog(object):
    def __init__(self, parent, title, value, comment=None, entry_changed_callback=None, masked=False):

        self.builder = Gtk.Builder()
        self.builder.set_translation_domain(APP_NAME)
        self.builder.add_from_file(EDIT_DIALOG_UI_FILE)
        # self.builder.connect_signals(self)
        self.dialog = self.builder.get_object("dialog")                      # type: Gtk.Dialog
        self.dialog.set_transient_for(parent)
        self.dialog.set_title(title)
        self.apply_button = self.builder.get_object("apply-button")          # type: Gtk.Button
        self.show_checkbutton = self.builder.get_object("show-checkbutton")  # type: Gtk.CheckButton

        self.label = self.builder.get_object("comment")                      # type: Gtk.Label
        if comment:
            self.label.set_label(comment)

        self.entry = self.builder.get_object("entry")                        # type: Gtk.Entry
        self.entry.set_text(str(value))
        if entry_changed_callback:
            self.entry.connect("changed", entry_changed_callback, self)
        if masked:
            self.entry.set_visibility(False)
            self.show_checkbutton.show()
            self.show_checkbutton.connect("toggled", self.on_show_button_toggled)
            self.dialog.connect("key-press-event", self.on_key_pressed)

    def run(self) -> Gtk.ResponseType:
        return self.dialog.run()

    def destroy(self):
        self.dialog.destroy()

    def on_show_button_toggled(self, button, data=None):
        self.entry.set_visibility(button.get_active())

    def on_key_pressed(self, widget, event):
        # Copy the selection of the entry to the clipboard if Ctrl-c is pressed
        if event.keyval == Gdk.KEY_c and event.state & Gdk.ModifierType.CONTROL_MASK:
            self.entry.copy_clipboard()