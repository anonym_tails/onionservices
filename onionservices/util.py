import os
import threading
import re
from logging import getLogger

from gi.repository import GLib

logger = getLogger(__name__)


def run_threaded(func, *args):
    """Run the specified function in a new thread"""
    thread = threading.Thread(target=run_with_exception_handling, args=(func, *args))
    thread.start()


def run_with_exception_handling(func, *args):
    try:
        func(*args)
    except Exception as e:
        logger.exception(e)


def run_threaded_when_idle(func, *args):
    """Run the specified function wrapped in a GLib.idle_add() call in a new thread"""
    run_threaded(GLib.idle_add, func, *args)


def tails_persistence_available():
    return os.path.exists("/live/persistence/TailsData_unlocked")


def process_mainloop_events():
    context = GLib.MainLoop().get_context()
    while context.pending():
        context.iteration()


def camel_to_snake_case(s: str) -> str:
    return re.sub("([A-Z])", "_\\1", s).lower().lstrip("_")
