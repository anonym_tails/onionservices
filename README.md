onionservices
============

A graphical interface for [onionkit](https://gitlab.com/segfault_/onionkit)


Installation
============

    sudo ./setup.py install

Usage
=====

    onionservices
